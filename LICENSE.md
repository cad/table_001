[En]
## DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
Version 2, December 2004

Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

Everyone is permitted to copy and distribute verbatim or modified copies of this license document, and changing it is allowed as long as the name is changed.

### DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0.You just DO WHAT THE FUCK YOU WANT TO.

[Fr]
## LICENCE PUBLIQUE RIEN À BRANLER
Copyright (C) 2009 Sam Hocevar <sam@hocevar.net>

14 rue de Plaisance, 75014 Paris, France
 
La copie et la distribution de copies exactes de cette licence sont autorisées, et toute modification est permise à condition de changer le nom de la licence. 

### LICENCE PUBLIQUE RIEN À BRANLER
CONDITIONS DE COPIE, DISTRIBUTON ET MODIFICATION

  0.Faites ce que vous voulez, j’en ai RIEN À BRANLER.
