# Table 6/8 personnes. Plateau bois, pieds acier.

![vue 3d freecad](https://framagit.org/cad/table_001/raw/master/img/table_001_screenshot_1.png)
![vue 3d freecad](https://framagit.org/cad/table_001/raw/master/img/DSC05900.jpg)
*Plus de photos dans le dossier [img](/img)*

Conception sous [FreeCad](http://freecadweb.org/), plans repris sous [Draftsight](http://www.3ds.com/products-services/draftsight-cad-software/).   
*Pièces 3d dans le dossier [3d](/3d)*

## V0.1 - 12/2016

### Nomenclature

| Rep | Désignation | Matière                         | Dim(mm) / Qté | Prix*     |
| --- |------------ | ------------------------------- |:-------------:| ---------:|
| 1   | Plateau     | Pin                             | 2000x800x35   | 70,00€    |
| 2   | Pieds       | Acier (S235)                    |               |           |
| 2.1 | pieds 1     | Tube rectangulaire 60x30x2      | 734mm / Qté 4 | 22,13€/6m |
| 2.2 | pieds 2     | Tube rectangulaire 60x30x2      | 800mm / Qté 2 |           |
| 2.3 | plat_sup    | Fer plat 60x6                   | 800mm / Qté 2 | 21,84€/6m |
| 2.4 | plat_inf    | Fer plat 60x6                   | 760mm / Qté 2 |           |
| 3   | Vis         | Acier têtes bombées larges T30  | 6x30  / Qté 6 | 6,69€/50  |

**Prix indicatifs HT (plateau et quincaillerie magasin de bricolage, profilés quincaillerie industrielle). Prix de revient TTC pour la réalisation (hors consommables, disques, éléctrodes...) = 105,00€*

### Matériel
  * Mètre, équerre, fausse-équerre, crayon, pointe à tracer, pointeau
  * Meuleuse + disques à tronçonner acier + disques à meuler
  * Poste à souder (arc) + éléctrodes enrobées (D2,4)
  * Scie circulaire
  * Ciseau à bois
  * serre-joints / pinces
  * Perçeuse (foret D7 acier, D2 bois)
  * Ponceuse
  * Mastic (silicone, scnv...)
  * ...

### Réalisation

**W.I.P... à détailler...**

  1. Tracer *(mètre, équerre, pointe à tracer)* et débiter les tubes/plats *(meuleuse, disque à tronçonner)*
    * Tubes:...
    * Plats:...
    * Encoches:...
  2. Construire un gabarit d'assemblage *(planche, tasseaux, vis)* aux cotes extérieures, et y reporter *(crayon)* les épaisseurs du plat inférieur et du plateau
  3. Présenter les pièces *(pieds, plat inf)*, vérifier les cotes et retoucher
  4. Brider *(serres-joints, pinces)* et souder.
    * Pointer. Retourner, brider. Pointer. Retourner. Souder...
  5. Tracer les encoches du plateau *(mètre, règle, crayon)*
  6. Réaliser les encoches *(scie circulaire, ciseau à bois)*
  7. Retourner le plateau, présenter les pieds. Réaliser des trous de fixation *(perceuse, foret acier D7)* dans le plat inférieur, et visser les pieds
  8. Poncer le plateau *(chants, coins)*
  9. Coller les plats supérieurs
  10. Nettoyer, traiter
  
### Commentaires pour la V0.2
  * La table a un peu de jeu dans la longueur, dû au fait de la torsion que peuvent prendre les plats inférieurs (pas gênant), prévoir du calage dans les encoches des pieds (ou des plats inférieurs plus épais + vis de fixation plus vers l'extérieur).
  * Le tube rectangulaire est trop fin (ép. 2mm), difficulté de soudage (ça "perce"), prévoir plus épais.

### Licence
[Licence Puplique R.À.B](LICENSE.md) ([WTFPL](http://www.wtfpl.net/))
  
